from django.db import models


class Dog(models.Model):
    name = models.TextField()
    age = models.IntegerField()
    does_bark = models.BooleanField()
    bark_sound = models.TextField(default="Bark!")

    def make_sound(self):
        if self.does_bark:
            return self.bark_sound
        return "I'm a sad dog that doesn't bark at all!"

    def __str__(self):
        return f"{self.name} ({self.age})"
