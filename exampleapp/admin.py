from django.contrib import admin

from exampleapp.models import Dog


@admin.register(Dog)
class DogAdmin(admin.ModelAdmin):
    pass
