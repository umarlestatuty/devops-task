from django.http import HttpResponse

from exampleapp.models import Dog


def sample_view(request):
    return HttpResponse("<h1>It works!</h1>", status=200)


def all_of_my_dogs(request):
    content = "<h1>Moje pieski:</h1><img width=300px src='/static/img/piesek.jpeg'><table>"
    content += "<tr><th>Imię</th><th>Wiek</th><th>Szczeka?</th><th>Jak szczeka?</th></tr>"
    for dog in Dog.objects.all():
        content += f"<tr><td><b>{dog.name}</b></td><td>{dog.age}</td><td>{'Tak!' if dog.does_bark else 'Nie :c'}</td><td>{dog.make_sound()}</td></tr>"
    content += "</table>"
    return HttpResponse(content, status=418)
