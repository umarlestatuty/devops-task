from django.test import TestCase

from exampleapp.models import Dog


class DogTestCase(TestCase):
    def setUp(self) -> None:
        pass

    def test_make_sound(self):
        bark_sound = "test bark sound"

        dog_1 = Dog(name="Test1", age=10, does_bark=True, bark_sound=bark_sound)
        self.assertEquals(bark_sound, dog_1.make_sound())

        dog_2 = Dog(name="Test2", age=20, does_bark=False, bark_sound=bark_sound)
        self.assertEquals("I'm a sad dog that doesn't bark at all!", dog_2.make_sound())
