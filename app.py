#!/usr/bin/env python
import re
import subprocess
import sys

image_name = "sus-devops"


def main():
    del sys.argv[0]  # Remove first argument (filename)

    if len(sys.argv) == 0:
        print_help()
    else:
        if sys.argv[0] == "install":
            docker_install()
            if len(sys.argv) > 1 and sys.argv[1] == "run":
                docker_run()
        elif sys.argv[0] == "run":
            docker_run()
        elif sys.argv[0] == "stop":
            docker_stop()
        elif sys.argv[0] == "help":
            print_help()


def print_help():
    print(
        "=== Devops task ===\n",
        "Syntax: 'app.py <subcommand>'\n\n"
        "Subcommands:\n",
        "\tinstall - builds the image\n"
        "\trun - runs the container\n"
        "\tinstall run - builds, then runs the image\n"
        "\tstop - stops the container\n"
        "\thelp - shows this help."
    )


def docker_install():
    command = f"docker build -t {image_name} ."
    print(f"Running '{command}'")
    subprocess.run(command.split(" "))


def docker_run():
    command = f"docker run -d -p 8082:8082 {image_name}"
    print(f"Running '{command}'")
    subprocess.run(command.split(" "))


def docker_stop():
    command = "docker ps"
    print("Finding correct container...")
    img_id_search = re.search(fr"([\da-z\S]+)\s+?{image_name}", subprocess.check_output(command.split(" ")).decode())
    if img_id_search is not None and len(img_id_search.groups()) > 0:
        img_id = img_id_search.group(1)
        print(f"Found container (id {img_id}). Stopping.")
        command = f"docker stop {img_id}"
        subprocess.run(command)
    else:
        print("No running container found.")


if __name__ == '__main__':
    main()
