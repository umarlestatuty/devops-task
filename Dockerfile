FROM python:3.9-slim-buster
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
RUN python manage.py migrate
RUN python manage.py collectstatic --noinput
EXPOSE 8082
STOPSIGNAL SIGTERM
CMD ["gunicorn", "ExampleDjangoProject.wsgi", "--bind", "0.0.0.0:8082"]